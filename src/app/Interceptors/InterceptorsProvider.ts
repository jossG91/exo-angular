import { HTTP_INTERCEPTORS } from "@angular/common/http";

import { JwtInterceptor } from "./JwtInterceptor";

export const httpInterceptorProvider = [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
]