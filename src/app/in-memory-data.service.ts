import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { User } from './User/User';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const users: User[] = [
      { id: 11, name: 'Dr Nice', type: 'SURGEON', birthdate: "1990-05-12", country: "Fr", isReady: true },
      { id: 12, name: 'Dr Nice', type: 'OPERATOR', birthdate: "1990-05-12", country: "Fr", isReady: true },
      { id: 13, name: 'Dr Nice', type: 'SURGEON', birthdate: "1990-05-12", country: "Fr", isReady: false },
      { id: 14, name: 'Dr Nice', type: 'SURGEON', birthdate: "1990-05-12", country: "Fr", isReady: true },
      { id: 15, name: 'Dr Nice', type: 'ADMIN', birthdate: "1990-05-12", country: "Fr", isReady: true },
      { id: 16, name: 'Dr Nice', type: 'SURGEON', birthdate: "1990-05-12", country: "Fr", isReady: true },
      { id: 17, name: 'Dr Nice', type: 'OPERATOR', birthdate: "1990-05-12", country: "Fr", isReady: true },
      { id: 18, name: 'Dr Nice', type: 'SURGEON', birthdate: "1990-05-12", country: "Fr", isReady: false },
      { id: 19, name: 'Dr Nice', type: 'ADMIN', birthdate: "1990-05-12", country: "Fr", isReady: true },
      { id: 20, name: 'Dr Nice', type: 'SURGEON', birthdate: "1990-05-12", country: "Fr", isReady: true },
    ];
    return {users};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(users: User[]): number {
    return users.length > 0 ? Math.max(...users.map(user => user.id)) + 1 : 11;
  }
}
