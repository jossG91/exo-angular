export interface User {
    id: number;
    name: string;
    type: string;
    birthdate: string;
    country: string;
    isReady: boolean;
}

