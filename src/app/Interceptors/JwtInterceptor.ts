import { Injectable } from "@angular/core";
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from "@angular/common/http";

import { Observable } from "rxjs";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>, next: HttpHandler)
    {
        const authToken = 'nullToken';

        const authRequest = request.clone({
            setHeaders: {'Autorization': authToken}
        });

        return next.handle(authRequest);
    }
}