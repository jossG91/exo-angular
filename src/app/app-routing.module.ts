import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './User/user.component';
import { DetailsComponent } from './User/Details/details.component';

const routes: Routes = [
  { path: 'users', component: UserComponent },
  { path: 'users/:id', component: DetailsComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }