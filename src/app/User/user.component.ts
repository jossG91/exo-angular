import { Component, OnInit } from '@angular/core';
import { User } from './User';
import { UserService } from './Service/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {
  users: User[] = [];
  selectedUser?: User;

  constructor(private userService: UserService) { }

  ngOnInit(): void 
  {
    this.getUsers();
  }

  getUsers(): void
  {
    this.userService.getAll()
      .subscribe(users => this.users = users);
  }

  onSelect(user: User): void 
  {
    this.selectedUser = user;
  }

}
