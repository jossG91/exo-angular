import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from 'src/app/Messages/Service/message.service';
import { Observable } from 'rxjs';
import { User } from '../User';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userUrl = 'api/users';

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) { }

  getAll(): Observable<User[]>
  {
    const users = this.http.get<User[]>(this.userUrl);
    this.log('get list');

    return users;
  }

  getOne(id: number): Observable<User>
  {
    return this.http.get<User>(`${this.userUrl}/${id}`);
  }

  update(user: User)
  {
    const options = {
      headers: new HttpHeaders({
        'Content-type': 'application/json'
      })
    }
    const body = JSON.stringify(user);
    return this.http.patch(`${this.userUrl}/${user.id}`,
      body,
      options
    );
  }

  private log(message: string): void
  {
    this.messageService.add(`User service: ${message}`);
  }
}
