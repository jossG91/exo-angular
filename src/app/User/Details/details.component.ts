import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UserService } from '../Service/user.service';
import { User } from '../User';

@Component({
  selector: 'app-user-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})

export class DetailsComponent implements OnInit {

  user?: User;
  userTypes = [
    "SURGEON",
    "ADMIN",
    "OPERATOR"
  ];

  isEditing: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.getUser();
  }

  private getUser(): void
  {
    const id = Number(this.route.snapshot.paramMap.get('id'));

    this.userService.getOne(id)
      .subscribe(user => this.user = user);
  }

  edit():void
  { 
  }

}
